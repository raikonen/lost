<?php

use App\Http\Controllers\AirportsController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\FlightController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1/'], function () {
    Route::post('airports/import', [AirportsController::class, 'import']);
    Route::post('flights/import', [FlightController::class, 'import']);
    Route::post('cities', [CitiesController::class, 'store']);

    Route::get('flights/cheapest', [FlightController::class, 'cheapest']);

    Route::get('cities', [CitiesController::class, 'index']);

    Route::post('cities/{id}/comments', [CommentsController::class, 'store']);
    Route::put('comments/{id}', [CommentsController::class, 'update']);
    Route::delete('comments/{id}', [CommentsController::class, 'destroy']);
});
