<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flights';
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $attributes = [
        'airline',
        'source_airport',
        'source_airport_id',
        'destination_airport',
        'destination_airport_id',
        'codeshare',
        'stops',
        'equipment',
        'price',
    ];

    public function destinationAirport()
    {
        return $this->hasOne(Airport::class, 'id', 'destination_airport_id');
    }

    public function sourceAirport()
    {
        return $this->hasOne(User::class, 'id', 'source_airport_id');
    }
}
