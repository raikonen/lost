<?php

namespace App\Providers;

use App\Airport;
use App\City;
use App\Flight;
use App\Services\ImportService\AirportsImporter;
use App\Services\ImportService\AirportsImporterInterface;
use App\Services\ImportService\RoutesImporter;
use App\Services\ImportService\RoutesImporterInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AirportsImporterInterface::class, function () {
            return new AirportsImporter(
                function (array $airports) {
                    Airport::insert($airports);
                },
                function (array $cities) {
                    City::insertOrIgnore($cities);
                },
                function () {
                    return City::all()->keyBy('name')->toArray();
                }
            );
        });

        $this->app->bind(RoutesImporterInterface::class, function () {
            return new RoutesImporter(
                function () {
                    return Airport::all()->keyBy('airport_id')->toArray();
                },
                function (array $flights) {
                    City::insertOrIgnore($flights);
                }
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
