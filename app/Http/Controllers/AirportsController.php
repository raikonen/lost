<?php

namespace App\Http\Controllers;

use App\Airport;
use App\Services\ImportService\AirportsImporterInterface;
use Illuminate\Http\Request;

class AirportsController extends Controller
{
    private $importer;

    public function __construct(
        AirportsImporterInterface $importer
    ) {
        $this->importer = $importer;
    }

    public function import(Request $request)
    {
        $file = $request->file('import');

        if (!$file->isValid()) {
            return response('Not valid file.', 400);
        }

        $this
            ->importer
            ->import(
                $file->openFile('r')
            );

        return response()
            ->json(
                null,
                200
            );
    }
}
