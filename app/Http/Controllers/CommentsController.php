<?php

namespace App\Http\Controllers;

use App\City;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(int $id, Request $request)
    {
        $city = City::findOrFail($id);

        $request->validate([
            'content' => 'required',
        ]);

        Comment::create([
            'city_id' => $city->id,
            'content' => $request->input('content'),
            'created_by_user_id' => 1,
        ]);

        return response()
            ->json(
            'Comment created',
            201
            );
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->all());

        return response()
            ->json(
                $comment,
                200
            );
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response()
            ->json(
                null,
                204
            );
    }
}
