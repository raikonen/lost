<?php

namespace App\Http\Controllers;

use App\Services\ImportService\RoutesImporterInterface;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    private $importer;

    public function __construct(
        RoutesImporterInterface $importer
    ) {
        $this->importer = $importer;
    }

    public function cheapest(Request $request)
    {
        //
    }

    public function import(Request $request)
    {
        $file = $request->file('import');

        if (!$file->isValid()) {
            return response('Not valid file.', 400);
        }

        $this
            ->importer
            ->import(
                $file->openFile('r')
            );

        return response()
            ->json(
                null,
                200
            );
    }
}
