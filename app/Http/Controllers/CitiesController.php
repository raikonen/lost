<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Resources\CityCollection;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('name')) {
            $cities = City::with('comments')
                ->where(
                    'name',
                    'like',
                    "%{$request->input('name')}%"
                )->get();
        } else {
            $cities = City::with('comments')
                ->get();
        }

        return new CityCollection($cities);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cities',
            'country' => 'required',
            'description' => 'required',
        ]);

        $city = City::create($request->all());

        return response()
            ->json(
                $city,
                201
            );
    }
}
