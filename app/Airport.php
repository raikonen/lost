<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $table = 'airports';
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $attributes = [
        'airport_id',
        'name',
        'city_id',
        'country',
        'iata',
        'icao',
        'latitude',
        'longitude',
        'altitude',
        'timezone',
        'dts',
        'tz',
        'type',
        'source',
    ];

    public function departingFlights()
    {
        return $this->hasMany(Flight::class, 'source_airport_id');
    }

    public function arrivingFlights()
    {
        return $this->hasMany(Flight::class, 'destination_airport_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
