<?php


namespace App\Services\ImportService;


use SplFileObject;

class RoutesImporter implements RoutesImporterInterface
{
    private $airports_finder;
    private $flights_repository;
    private $airports;

    public function __construct(
        callable $airports_finder,
        callable $flights_repository
    ) {
        $this->airports_finder = $airports_finder;
        $this->flights_repository = $flights_repository;
    }

    public function import(SplFileObject $file)
    {
        $this->airports = call_user_func($this->airports_finder);

        $routes = [];
        while ($data = $file->fgetcsv()) {
            if (isset($this->airports[$data[3]]) && isset($this->airports[$data[5]])) {
                $routes[] = [
                    'airline' => $data[0],
                    'source_airport' => $data[1],
                    'source_airport_id' => $this->airports[$data[3]]->id,
                    'destination_airport' => $data[3],
                    'destination_airport_id' => $this->airports[$data[4]]->id,
                    'codeshare' => $data[5],
                    'stops' => $data[6],
                    'equipment' => $data[7],
                    'price' => $data[0],
                ];
            }
        }

        if (!empty($routes)) {
            foreach (array_chunk($routes, 500) as $chunk) {
                call_user_func(
                    $this->flights_repository,
                    $chunk
                );
            }
        }
    }
}
