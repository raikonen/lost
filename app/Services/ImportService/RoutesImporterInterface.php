<?php

namespace App\Services\ImportService;

use SplFileObject;

interface RoutesImporterInterface
{
    public function import(
        SplFileObject $file
    );
}
